package ru.omsu.imit.troeglazova.rest.response;

import ru.omsu.imit.troeglazova.model.Candidate;
import ru.omsu.imit.troeglazova.model.User;

import java.util.Objects;

public class VoteResponse {

    private int id;
    private User user;
    private Candidate candidate;

    public VoteResponse(int id, User user, Candidate candidate) {
        this.id = id;
        this.user = user;
        this.candidate = candidate;
    }

    public VoteResponse(User user, Candidate candidate) {
        this(0, user, candidate);
    }

    public VoteResponse(int userId, String userFirstName, String userLastName, String userPatronymic, String userPhoneNumber,
                       User candidateUser, String candidateProgram ) {
        this(new User(userId, userFirstName, userLastName, userPatronymic, userPhoneNumber),
                new Candidate(candidateUser, candidateProgram));
    }

    public VoteResponse(int id, int userId, String userFirstName, String userLastName, String userPatronymic, String userPhoneNumber,
                       User candidateUser, String candidateProgram ) {
        this(id, new User(userId, userFirstName, userLastName, userPatronymic, userPhoneNumber),
                new Candidate(candidateUser, candidateProgram));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VoteResponse that = (VoteResponse) o;
        return id == that.id &&
                Objects.equals(user, that.user) &&
                Objects.equals(candidate, that.candidate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, candidate);
    }
}