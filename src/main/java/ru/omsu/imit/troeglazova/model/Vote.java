package ru.omsu.imit.troeglazova.model;

import java.util.Objects;

public class Vote {
    private int id;
    private User user;
    private Candidate candidate;

    public Vote(int id, User user, Candidate candidate) {
        this.id = id;
        this.user = user;
        this.candidate = candidate;
    }

    public Vote(User user, Candidate candidate) {
        this(0, user, candidate);
    }

    public Vote(int userId, String userFirstName, String userLastName, String userPatronymic, String userPhoneNumber,
                User candidateUser, String candidateProgram ) {
        this(new User(userId, userFirstName, userLastName, userPatronymic, userPhoneNumber),
                new Candidate(candidateUser, candidateProgram));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vote vote = (Vote) o;
        return id == vote.id &&
                Objects.equals(user, vote.user) &&
                Objects.equals(candidate, vote.candidate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, candidate);
    }
}
