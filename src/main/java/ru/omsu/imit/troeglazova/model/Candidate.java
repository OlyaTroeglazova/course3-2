package ru.omsu.imit.troeglazova.model;

import java.util.Objects;

public class Candidate {
    private User user;
    private String program;

    public Candidate(User user, String program) {
        this.user = user;
        this.program = program;
    }

    public Candidate(int id, String firstName, String lastName, String patronymic, String phoneNumber, String program) {
        this.user = new User(id, firstName, lastName, patronymic, phoneNumber);
        this.program = program;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Candidate candidate = (Candidate) o;
        return Objects.equals(user, candidate.user) &&
                Objects.equals(program, candidate.program);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, program);
    }
}
