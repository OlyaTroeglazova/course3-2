package ru.omsu.imit.troeglazova.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.troeglazova.dao.CandidateDAO;
import ru.omsu.imit.troeglazova.exception.VotingException;
import ru.omsu.imit.troeglazova.model.Candidate;
import ru.omsu.imit.troeglazova.model.User;
import ru.omsu.imit.troeglazova.utils.ErrorCode;

public class CandidateDAOImpl extends BaseDAOImpl implements CandidateDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(CandidateDAOImpl.class);

    @Override
    public Candidate insertCandidate(Candidate candidate) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            getCandidateMapper(sqlSession).insertCandidate(candidate);
            sqlSession.commit();
            return candidate;
        } catch (RuntimeException ex) {
            LOGGER.debug(ex.toString());
            LOGGER.debug("Can't insert Candidate {}", ex);
            throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
        }
    }

    @Override
    public Candidate getCandidateById(int id) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            sqlSession.commit();
            return getCandidateMapper(sqlSession).getCandidateById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get Candidate {}", ex);
            throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
        }
    }

    @Override
    public void deleteCandidateById(int id) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            getCandidateMapper(sqlSession).deleteCandidateById(id);
            sqlSession.commit();
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't delete Candidate {}", ex);
            throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
        }
    }

    @Override
    public void editCandidateById(int id, User user, String program) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getCandidateMapper(sqlSession).editCandidateById(id, user, program);
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't change Candidate {} {} ", id, ex);
                sqlSession.rollback();
                throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
            }
        }
    }

    @Override
    public void deleteAllCandidates() throws VotingException {
        LOGGER.debug("Delete All Candidates {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getCandidateMapper(sqlSession).deleteAllCandidates();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Candidates {}", ex);
                sqlSession.rollback();
                throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
            }
            sqlSession.commit();
        }
    }
}
