package ru.omsu.imit.troeglazova.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.troeglazova.dao.UserDAO;
import ru.omsu.imit.troeglazova.exception.VotingException;
import ru.omsu.imit.troeglazova.model.User;
import ru.omsu.imit.troeglazova.utils.ErrorCode;

import java.util.List;

public class UserDAOImpl extends BaseDAOImpl implements UserDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDAOImpl.class);

    @Override
    public User insertUser(User user) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            getUserMapper(sqlSession).insertUser(user);
            sqlSession.commit();
            return user;
        } catch (RuntimeException ex) {
            LOGGER.debug(ex.toString());
            LOGGER.debug("Can't insert User {}", ex);
            throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
        }
    }

    @Override
    public User getUserById(int id) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            sqlSession.commit();
            return getUserMapper(sqlSession).getUserById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get User {}", ex);
            throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
        }
    }

    @Override
    public void deleteUserById(int id) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            getUserMapper(sqlSession).deleteUserById(id);
            sqlSession.commit();
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't delete User {}", ex);
            throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
        }
    }

    @Override
    public void editUserById(int id, String firstName, String lastName, String patronymic, String phoneNumber) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).editUserById(id, firstName, lastName, patronymic, phoneNumber);
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't change User {} {} ", id, ex);
                sqlSession.rollback();
                throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
            }
        }
    }

    @Override
    public void deleteAllUsers() throws VotingException {
        LOGGER.debug("Delete All Users {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).deleteAllUsers();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Users {}", ex);
                sqlSession.rollback();
                throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
            }
            sqlSession.commit();
        }
    }

    @Override
    public List<User> getUsersByCandidateId(int candidateId) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            try {
                return getUserMapper(sqlSession).getUsersByCandidateId(candidateId);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't get Users {} {} ", candidateId, ex);
                throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
            }
        }
    }
}
