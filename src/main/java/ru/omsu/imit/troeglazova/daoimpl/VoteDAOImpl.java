package ru.omsu.imit.troeglazova.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.troeglazova.dao.VoteDAO;
import ru.omsu.imit.troeglazova.exception.VotingException;
import ru.omsu.imit.troeglazova.model.Candidate;
import ru.omsu.imit.troeglazova.model.Vote;
import ru.omsu.imit.troeglazova.model.User;
import ru.omsu.imit.troeglazova.utils.ErrorCode;

public class VoteDAOImpl extends BaseDAOImpl implements VoteDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(VoteDAOImpl.class);

    @Override
    public Vote insertVote(Vote vote) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            getVoteMapper(sqlSession).insertVote(vote);
            sqlSession.commit();
            return vote;
        } catch (RuntimeException ex) {
            LOGGER.debug(ex.toString());
            LOGGER.debug("Can't insert Vote {}", ex);
            throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
        }
    }

    @Override
    public Vote getVoteById(int id) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            sqlSession.commit();
            return getVoteMapper(sqlSession).getVoteById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get Vote {}", ex);
            throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
        }
    }

    @Override
    public void deleteVoteById(int id) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            getVoteMapper(sqlSession).deleteVoteById(id);
            sqlSession.commit();
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't delete Vote {}", ex);
            throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
        }
    }

    @Override
    public void editVoteById(int id, User user, Candidate candidate) throws VotingException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getVoteMapper(sqlSession).editVoteById(id, user, candidate);
                sqlSession.commit();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't change Vote {} {} ", id, ex);
                sqlSession.rollback();
                throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
            }
        }
    }

    @Override
    public void deleteAllVotes() throws VotingException {
        LOGGER.debug("Delete All Votes {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getVoteMapper(sqlSession).deleteAllVotes();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Votes {}", ex);
                sqlSession.rollback();
                throw new VotingException(ex, ErrorCode.UNKNOWN_ERROR);
            }
            sqlSession.commit();
        }
    }
}
