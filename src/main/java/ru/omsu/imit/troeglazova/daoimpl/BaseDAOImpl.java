package ru.omsu.imit.troeglazova.daoimpl;

import org.apache.ibatis.session.SqlSession;
import ru.omsu.imit.troeglazova.mappers.*;
import ru.omsu.imit.troeglazova.utils.MyBatisUtils;

public class BaseDAOImpl {

    protected SqlSession getSession() {
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    protected VoteMapper getVoteMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(VoteMapper.class);
    }

    protected UserMapper getUserMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(UserMapper.class);
    }

    protected CandidateMapper getCandidateMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(CandidateMapper.class);
    }
}
