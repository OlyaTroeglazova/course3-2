package ru.omsu.imit.troeglazova.dao;

import ru.omsu.imit.troeglazova.exception.VotingException;
import ru.omsu.imit.troeglazova.model.User;

import java.util.List;

public interface UserDAO {

    User insertUser(User user) throws VotingException;

    User getUserById(int id) throws VotingException;

    void deleteUserById(int id) throws VotingException;

    void editUserById(int id, String firstName, String lastName, String patronymic, String phoneNumber) throws VotingException;

    void deleteAllUsers() throws VotingException;

    List<User> getUsersByCandidateId(int roomId) throws VotingException;
}
