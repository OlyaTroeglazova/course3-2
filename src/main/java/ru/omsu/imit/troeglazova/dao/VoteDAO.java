package ru.omsu.imit.troeglazova.dao;

import ru.omsu.imit.troeglazova.exception.VotingException;
import ru.omsu.imit.troeglazova.model.Candidate;
import ru.omsu.imit.troeglazova.model.Vote;
import ru.omsu.imit.troeglazova.model.User;

public interface VoteDAO {

    Vote insertVote(Vote vote) throws VotingException;

    Vote getVoteById(int id) throws VotingException;

    void deleteVoteById(int id) throws VotingException;

    void editVoteById(int id, User user, Candidate candidate) throws VotingException;

    void deleteAllVotes() throws VotingException;
}
