package ru.omsu.imit.troeglazova.dao;

import ru.omsu.imit.troeglazova.exception.VotingException;
import ru.omsu.imit.troeglazova.model.Candidate;
import ru.omsu.imit.troeglazova.model.User;

public interface CandidateDAO {

    Candidate insertCandidate(Candidate candidate) throws VotingException;

    Candidate getCandidateById(int id) throws VotingException;

    void deleteCandidateById(int id) throws VotingException;

    void editCandidateById(int id, User user, String program) throws VotingException;

    void deleteAllCandidates() throws VotingException;
}
