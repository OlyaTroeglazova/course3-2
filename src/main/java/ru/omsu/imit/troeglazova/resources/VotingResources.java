package ru.omsu.imit.troeglazova.resources;

import ru.omsu.imit.troeglazova.service.VotingService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/api")
public class VotingResources {

    private static VotingService votingService = new VotingService();

    @POST
    @Path("/voting/user/")
    @Consumes("application/json")
    @Produces("application/json")
    public Response insertUser(String json) {
        return votingService.insertUser(json);
    }

    @POST
    @Path("/voting/candidate/")
    @Consumes("application/json")
    @Produces("application/json")
    public Response insertCandidate(String json) {
        return votingService.insertCandidate(json);
    }

    @POST
    @Path("/voting/vote/")
    @Consumes("application/json")
    @Produces("application/json")
    public Response insertVote(String json) {
        return votingService.insertVote(json);
    }

    @GET
    @Path("/voting/user/{id}")
    @Produces("application/json")
    public Response getUserById(@PathParam(value = "id") int id) {
        return votingService.getUserById(id);
    }

    @GET
    @Path("/voting/candidate/{id}")
    @Produces("application/json")
    public Response getCandidateById(@PathParam(value = "id") int id) {
        return votingService.getCandidateById(id);
    }

    @GET
    @Path("/voting/vote/{id}")
    @Produces("application/json")
    public Response getVoteById(@PathParam(value = "id") int id) {
        return votingService.getVoteById(id);
    }

    @DELETE
    @Path("/voting/user/del/{id}")
    @Produces("application/json")
    public Response deleteUserById(@PathParam(value = "id") int id) {
        return votingService.deleteUserById(id);
    }

    @DELETE
    @Path("/voting/candidate/del/{id}")
    @Produces("application/json")
    public Response deleteCandidateById(@PathParam(value = "id") int id) {
        return votingService.deleteCandidateById(id);
    }

    @DELETE
    @Path("/voting/vote/del/{id}")
    @Produces("application/json")
    public Response deleteVoteById(@PathParam(value = "id") int id) {
        return votingService.deleteVoteById(id);
    }

    @PUT
    @Path("/voting/user/edit/{id}")
    @Produces("application/json")
    public Response editUserById(@PathParam(value = "id") int id, String json) {
        return votingService.editUserById(id, json);
    }

    @PUT
    @Path("/voting/candidate/edit/{id}")
    @Produces("application/json")
    public Response editCandidateById(@PathParam(value = "id") int id, String json) {
        return votingService.editCandidateById(id, json);
    }

    @PUT
    @Path("/voting/vote/edit/{id}")
    @Produces("application/json")
    public Response editVoteById(@PathParam(value = "id") int id, String json) {
        return votingService.editVoteById(id, json);
    }

    @DELETE
    @Path("/voting/user/del/")
    @Produces("application/json")
    public Response deleteAllUsers() {
        return votingService.deleteAllUsers();
    }

    @DELETE
    @Path("/voting/candidate/del/")
    @Produces("application/json")
    public Response deleteAllCandidates() {
        return votingService.deleteAllCandidates();
    }

    @DELETE
    @Path("/voting/vote/del/")
    @Produces("application/json")
    public Response deleteAllVotes() {
        return votingService.deleteAllVotes();
    }

    @GET
    @Path("/voting/user/getByCandidateId/{id}")
    @Produces("application/json")
    public Response getUsersByCandidateId(@PathParam(value = "id") int id) {
        return votingService.getUsersByCandidateId(id);
    }

}
