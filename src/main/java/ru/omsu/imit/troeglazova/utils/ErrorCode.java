package ru.omsu.imit.troeglazova.utils;

public enum ErrorCode {

    SUCCESS(""),
    ITEM_NOT_FOUND("Item not found %s"),
    NULL_REQUEST("Null request"),
    JSON_PARSE_EXCEPTION("Json parse exception : %s"),
    WRONG_URL("Wrong URL"),
    METHOD_NOT_ALLOWED("Method not allowed"),
    UNKNOWN_ERROR("Unknown error"),
    WRONG_PARAMS("Wrong params");

    private String message;

    ErrorCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
