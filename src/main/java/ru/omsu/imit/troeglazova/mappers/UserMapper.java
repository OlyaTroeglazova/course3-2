package ru.omsu.imit.troeglazova.mappers;

import org.apache.ibatis.annotations.*;
import ru.omsu.imit.troeglazova.model.User;

import java.util.List;

public interface UserMapper {

    @Insert("INSERT INTO USER(first_name, last_name, patronymic, phone_number) " +
            "VALUES (#{firstName},#{lastName},#{patronymic},#{phoneNumber})")
    @Options(useGeneratedKeys = true)
    Integer insertUser(User user);

    @Select("SELECT id, first_name, last_name, patronymic, phone_number FROM USER WHERE id = #{id}")
    User getUserById(int id);

    @Delete("DELETE FROM USER WHERE id = #{id}")
    void deleteUserById(int id);

    @Update("UPDATE USER SET first_name = #{firstName}, last_name = #{lastName}, patronymic = #{patronymic}, phone_number = #{phoneNumber}" +
            " WHERE id = #{id} ")
    void editUserById(@Param("id") int id, @Param("firstName") String firstName,
                                @Param("lastName") String lastName, @Param("patronymic") String patronymic,
                                @Param("phoneNumber") String phoneNumber);

    @Delete("DELETE FROM USER")
    void deleteAllUsers();

    @Select("SELECT * FROM USER WHERE id IN(SELECT user_id FROM VOTE WHERE candidate_id = #{id})" )
    List<User> getUsersByCandidateId(@Param("id") int candidateId);
}
