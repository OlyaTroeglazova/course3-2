package ru.omsu.imit.troeglazova.mappers;

import org.apache.ibatis.annotations.*;
import ru.omsu.imit.troeglazova.model.Candidate;
import ru.omsu.imit.troeglazova.model.Vote;
import ru.omsu.imit.troeglazova.model.User;

public interface VoteMapper {

    @Insert("INSERT INTO VOTE(user_id, candidate_id) " +
            "VALUES (#{user.id},#{candidate.user.id})")
    @Options(useGeneratedKeys = true)
    Integer insertVote(Vote vote);

    @Select("SELECT VOTE.id, U1.id, U1.first_name, U1.last_name, U1.patronymic, U1.phone_number, " +
            "U2.id, U2.first_name, U2.last_name, U2.patronymic, U2.phone_number, program " +
            "FROM USER as U1, USER as U2, CANDIDATE, VOTE WHERE VOTE.user_id = U1.id && candidate_id = CANDIDATE.id" +
            "&& CANDIDATE.user_id = U2.id && VOTE.id = #{id})")
    Vote getVoteById(int id);

    @Delete("DELETE FROM VOTE WHERE id = #{id}")
    void deleteVoteById(int id);

    @Update("UPDATE VOTE SET user_id = #{user.id}, candidate_id = #{candidate.user.id}" +
            " WHERE id = #{id} ")
    void editVoteById(@Param("id") int id, @Param("user") User user,
                      @Param("candidate") Candidate candidate);

    @Delete("DELETE FROM VOTE")
    void deleteAllVotes();
}
