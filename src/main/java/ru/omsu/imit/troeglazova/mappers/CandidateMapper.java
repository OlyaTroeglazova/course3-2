package ru.omsu.imit.troeglazova.mappers;

import org.apache.ibatis.annotations.*;
import ru.omsu.imit.troeglazova.model.Candidate;
import ru.omsu.imit.troeglazova.model.User;

public interface CandidateMapper {

    @Insert("INSERT INTO CANDIDATE(user_id, program) " +
            "VALUES (#{user.id},#{program})")
    @Options(useGeneratedKeys = true)
    Integer insertCandidate(Candidate candidate);

    @Select("SELECT USER.id, USER.first_name, USER.last_name, USER.patronymic, USER.phone_number, program FROM CANDIDATE, USER WHERE CANDIDATE.user_id = USER.id && CANDIDATE.user_id = #{id}")
    Candidate getCandidateById(int id);

    @Delete("DELETE FROM CANDIDATE WHERE user_id = #{id}")
    void deleteCandidateById(int id);

    @Update("UPDATE CANDIDATE SET user_id = #{user.id}, program = #{program} WHERE user_id = #{user.id} ")
    void editCandidateById(@Param("id") int id, @Param("user") User user,  @Param("program") String program);

    @Delete("DELETE FROM CANDIDATE")
    void deleteAllCandidates();
}
