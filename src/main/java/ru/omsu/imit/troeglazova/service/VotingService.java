package ru.omsu.imit.troeglazova.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.troeglazova.dao.CandidateDAO;
import ru.omsu.imit.troeglazova.dao.VoteDAO;
import ru.omsu.imit.troeglazova.dao.UserDAO;
import ru.omsu.imit.troeglazova.daoimpl.*;
import ru.omsu.imit.troeglazova.exception.VotingException;
import ru.omsu.imit.troeglazova.model.*;
import ru.omsu.imit.troeglazova.rest.request.*;
import ru.omsu.imit.troeglazova.rest.response.*;
import ru.omsu.imit.troeglazova.utils.VotingUtils;
import ru.omsu.imit.troeglazova.utils.ErrorCode;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class VotingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VotingService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private VoteDAO voteDAO = new VoteDAOImpl();
    private UserDAO userDAO = new UserDAOImpl();
    private CandidateDAO candidateDAO = new CandidateDAOImpl();

    public Response insertUser(String json) {
        LOGGER.debug("Insert user item " + json);
        try {
            UserRequest request = VotingUtils.getClassInstanceFromJson(GSON, json, UserRequest.class);
            checkUserFields(request.getFirstName(), request.getLastName(), request.getPhoneNumber());
            User item = new User(request.getFirstName(), request.getLastName(), request.getPatronymic(), request.getPhoneNumber());
            User addedItem = userDAO.insertUser(item);
            String response = GSON.toJson(new UserResponse(addedItem.getId(), addedItem.getFirstName(), addedItem.getLastName(),
                    addedItem.getPatronymic(), addedItem.getPhoneNumber()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response insertCandidate(String json) {
        LOGGER.debug("Insert candidate item " + json);
        try {
            CandidateRequest request = VotingUtils.getClassInstanceFromJson(GSON, json, CandidateRequest.class);
            checkCandidateFields(request.getUser(), request.getProgram());
            Candidate item = new Candidate(request.getUser(), request.getProgram());
            Candidate addedItem = candidateDAO.insertCandidate(item);
            String response = GSON.toJson(new CandidateResponse(addedItem.getUser(), addedItem.getProgram()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response insertVote(String json) {
        LOGGER.debug("Insert vote item " + json);
        try {
            VoteRequest request = VotingUtils.getClassInstanceFromJson(GSON, json, VoteRequest.class);
            checkVoteFields(request.getUser(), request.getCandidate());
            Vote item = new Vote(request.getUser().getId(), request.getUser().getFirstName(), request.getUser().getLastName(),
                    request.getUser().getPatronymic(), request.getUser().getPhoneNumber(),
                    request.getCandidate().getUser(), request.getCandidate().getProgram());
            Vote addedItem = voteDAO.insertVote(item);
            String response = GSON.toJson(new VoteResponse(addedItem.getId(), addedItem.getUser().getId(), addedItem.getUser().getFirstName(), addedItem.getUser().getLastName(),
                    addedItem.getUser().getPatronymic(), addedItem.getUser().getPhoneNumber(),
                    addedItem.getCandidate().getUser(), addedItem.getCandidate().getProgram()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response getUserById(int id) {
        LOGGER.debug("get user by id " + id);
        try {
            User item = userDAO.getUserById(id);
            String response = GSON.toJson(new UserResponse(item.getId(), item.getFirstName(), item.getLastName(),
                    item.getPatronymic(), item.getPhoneNumber()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response getCandidateById(int id) {
        LOGGER.debug("get candidate by id " + id);
        try {
            Candidate item = candidateDAO.getCandidateById(id);
            String response = GSON.toJson(new CandidateResponse(item.getUser(), item.getProgram()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response getVoteById(int id) {
        LOGGER.debug("get vote by id " + id);
        try {
            Vote item = voteDAO.getVoteById(id);
            String response = GSON.toJson(new VoteResponse(item.getId(), item.getUser().getId(), item.getUser().getFirstName(), item.getUser().getLastName(),
                    item.getUser().getPatronymic(), item.getUser().getPhoneNumber(),
                    item.getCandidate().getUser(), item.getCandidate().getProgram()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response deleteUserById(int id) {
        LOGGER.debug("delete user by id " + id);
        try {
            userDAO.deleteUserById(id);
            String response = GSON.toJson(new EmptySuccessResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response deleteCandidateById(int id) {
        LOGGER.debug("delete candidate by id " + id);
        try {
            candidateDAO.deleteCandidateById(id);
            String response = GSON.toJson(new EmptySuccessResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response deleteVoteById(int id) {
        LOGGER.debug("delete vote by id " + id);
        try {
            voteDAO.deleteVoteById(id);
            String response = GSON.toJson(new EmptySuccessResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response editUserById(int id, String json) {
        LOGGER.debug("edit user by id " + id + json);
        try {
            UserRequest request = VotingUtils.getClassInstanceFromJson(GSON, json, UserRequest.class);
            checkUserFields(request.getFirstName(), request.getLastName(), request.getPhoneNumber());
            userDAO.editUserById(id, request.getFirstName(), request.getLastName(),
                    request.getPatronymic(), request.getPhoneNumber());
            String response = GSON.toJson(new UserResponse(id, request.getFirstName(), request.getLastName(),
                    request.getPatronymic(), request.getPhoneNumber()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response editCandidateById(int id, String json) {
        LOGGER.debug("edit candidate by id " + id + json);
        try {
            CandidateRequest request = VotingUtils.getClassInstanceFromJson(GSON, json, CandidateRequest.class);
            checkCandidateFields(request.getUser(), request.getProgram());
            candidateDAO.editCandidateById(id, request.getUser(), request.getProgram());
            String response = GSON.toJson(new CandidateResponse(id, request.getUser().getFirstName(), request.getUser().getLastName(), request.getUser().getPatronymic(),
                    request.getUser().getPhoneNumber(), request.getProgram()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response editVoteById(int id, String json) {
        LOGGER.debug("edit vote by id " + id + json);
        try {
            VoteRequest request = VotingUtils.getClassInstanceFromJson(GSON, json, VoteRequest.class);
            checkVoteFields(request.getUser(), request.getCandidate());
            voteDAO.editVoteById(id, request.getUser(),request.getCandidate());
            String response = GSON.toJson(new VoteResponse(id, request.getUser().getId(), request.getUser().getFirstName(), request.getUser().getLastName(),
                    request.getUser().getPatronymic(), request.getUser().getPhoneNumber(),
                    request.getCandidate().getUser(), request.getCandidate().getProgram()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response deleteAllUsers() {
        LOGGER.debug("delete all users");
        try {
            userDAO.deleteAllUsers();
            String response = GSON.toJson(new EmptySuccessResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response deleteAllCandidates() {
        LOGGER.debug("delete all candidates");
        try {
            candidateDAO.deleteAllCandidates();
            String response = GSON.toJson(new EmptySuccessResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response deleteAllVotes() {
        LOGGER.debug("delete all votes");
        try {
            voteDAO.deleteAllVotes();
            String response = GSON.toJson(new EmptySuccessResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    public Response getUsersByCandidateId(int id) {
        LOGGER.debug("get users by candidateId " + id);
        List<User> items = new ArrayList<>();
        try {
            items.addAll(userDAO.getUsersByCandidateId(id));
            List<UserResponse> responses = new ArrayList<>();
            for (User item : items) {
                responses.add(new UserResponse(item.getId(), item.getFirstName(), item.getLastName(), item.getPatronymic(), item.getPhoneNumber()));
            }
            String response = GSON.toJson(responses);
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (VotingException ex) {
            return VotingUtils.failureResponse(ex);
        }
    }

    private void checkUserFields(String firstName, String lastName, String phoneNumber) throws VotingException {
        if (firstName == null || firstName.length() == 0 || lastName == null || lastName.length() == 0 || phoneNumber == null || phoneNumber.length() != 11) {
            throw new VotingException(ErrorCode.WRONG_PARAMS);
        }
    }

    private void checkCandidateFields(User user, String program) throws VotingException {
        if ( user.getId() == 0 || program == null || program.length() == 0) {
            throw new VotingException(ErrorCode.WRONG_PARAMS);
        }
    }

    private void checkVoteFields(User user, Candidate candidate) throws VotingException {
        if (user.getId() == 0) {
            throw new VotingException(ErrorCode.WRONG_PARAMS);
        }
    }
}
