package ru.omsu.imit.troeglazova.exception;

import ru.omsu.imit.troeglazova.utils.ErrorCode;

public class VotingException extends Exception {

    private static final long serialVersionUID = 6049904777923589329L;
    private ErrorCode errorCode;
    private String param;

    public VotingException(ErrorCode errorCode, String param) {
        this.errorCode = errorCode;
        this.param = param;
    }

    public VotingException(ErrorCode errorCode) {
        this(errorCode, null);
    }

    public VotingException(Throwable cause, ErrorCode errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        if (param != null)
            return String.format(errorCode.getMessage(), param);
        else
            return errorCode.getMessage();
    }
}
