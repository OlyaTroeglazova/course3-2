package ru.omsu.imit.troeglazova.server.config;

import org.glassfish.jersey.server.ResourceConfig;

public class VotingResourceConfig extends ResourceConfig {
    public VotingResourceConfig() {
        packages("ru.omsu.imit.troeglazova.resources",
                "ru.omsu.imit.troeglazova.rest.mappers");
    }
}
