package ru.omsu.imit.troeglazova.clienttests;

import org.junit.Test;
import ru.omsu.imit.troeglazova.exception.VotingException;
import ru.omsu.imit.troeglazova.model.Candidate;
import ru.omsu.imit.troeglazova.model.User;
import ru.omsu.imit.troeglazova.rest.response.CandidateResponse;
import ru.omsu.imit.troeglazova.rest.response.VoteResponse;
import ru.omsu.imit.troeglazova.rest.response.UserResponse;
import ru.omsu.imit.troeglazova.utils.ErrorCode;

public class VotingTest extends BaseVotingTest {

    @Test
    public void testinsertUser() {

        insertUser("Ivan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
    }

    @Test
    public void testWronginsertUser1() {
        insertUser("", "Petrov", "Ivanovich", "89607584421", ErrorCode.WRONG_PARAMS);
    }

    @Test
    public void testWronginsertUser2() {
        insertUser("Ivan", "", "Ivanovich", "89607584421", ErrorCode.WRONG_PARAMS);
    }

    @Test
    public void testWronginsertUser3() {
        insertUser("Anton", "Petrov", "Ivanovich", "8960758441", ErrorCode.WRONG_PARAMS);
    }

    @Test
    public void testInsertCandidate1() {
        UserResponse userResponse = insertUser("Ivan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user = new User(userResponse.getId(), userResponse.getFirstName(), userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber());
        insertCandidate(user, "Good program", ErrorCode.SUCCESS);
    }

    @Test
    public void testWrongInsertCandidate1() {
        UserResponse userResponse = insertUser("Ivan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user = new User(userResponse.getId(), userResponse.getFirstName(), userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber());
        insertCandidate(user, "", ErrorCode.WRONG_PARAMS);
    }

    @Test
    public void testInsertVote() throws VotingException {
        UserResponse userResponse = insertUser("Ivan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user = new User(userResponse.getId(), userResponse.getFirstName(), userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber());
        CandidateResponse candidateResponse = insertCandidate(user, "Good program", ErrorCode.SUCCESS);
        Candidate candidate = new Candidate(candidateResponse.getUser(), candidateResponse.getProgram());
        insertVote(user, candidate, ErrorCode.SUCCESS);
    }

    @Test
    public void testGetUserById() {
        UserResponse userResponse = insertUser("Ivan", "Ivanov", "Ivanovich", "89134567232", ErrorCode.SUCCESS);
        getUserById(userResponse.getId(), userResponse.getFirstName(),
                userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber(), ErrorCode.SUCCESS);
    }

    @Test
    public void testGetCandidateById() {
        UserResponse userResponse = insertUser("Ivan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user = new User(userResponse.getId(), userResponse.getFirstName(), userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber());
        CandidateResponse candidateResponse = insertCandidate(user, "Good program", ErrorCode.SUCCESS);
       getCandidateById(candidateResponse.getUser().getId(),candidateResponse.getUser(), candidateResponse.getProgram(), ErrorCode.SUCCESS);
    }

    @Test
    public void testDeleteUserById() {
        UserResponse addResponse = insertUser("Ivan", "Ivanov", "Ivanovich", "89134567232", ErrorCode.SUCCESS);
        deleteUserById(addResponse.getId(), ErrorCode.SUCCESS);
    }

    @Test
    public void testDeleteCandidateById() {
        UserResponse userResponse = insertUser("Ivan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user = new User(userResponse.getId(), userResponse.getFirstName(), userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber());
        CandidateResponse candidateResponse = insertCandidate(user, "Good program", ErrorCode.SUCCESS);
        deleteCandidateById(candidateResponse.getUser().getId(), ErrorCode.SUCCESS);
    }

    @Test
    public void testDeleteVoteById() throws VotingException {
        UserResponse userResponse = insertUser("Ivan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user = new User(userResponse.getId(), userResponse.getFirstName(), userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber());
        CandidateResponse candidateResponse = insertCandidate(user, "Good program", ErrorCode.SUCCESS);
        Candidate candidate = new Candidate(candidateResponse.getUser(), candidateResponse.getProgram());
        VoteResponse voteResponse = insertVote(user, candidate, ErrorCode.SUCCESS);
        deleteVoteById(voteResponse.getId(), ErrorCode.SUCCESS);
    }

    @Test
    public void testEditUserById() {
        UserResponse addResponse = insertUser("Ivan", "Ivanov", "Ivanovich", "89134567232", ErrorCode.SUCCESS);
        editUserById(addResponse.getId(), "Petrov", addResponse.getLastName(), addResponse.getPatronymic(),
                addResponse.getPhoneNumber(), ErrorCode.SUCCESS);
    }

    @Test
    public void testWrongEditUserById() {
        UserResponse addResponse = insertUser("Ivan", "Ivanov", "Ivanovich", "89134567232", ErrorCode.SUCCESS);
        editUserById(addResponse.getId(), null, addResponse.getLastName(), addResponse.getPatronymic(),
                addResponse.getPhoneNumber(), ErrorCode.WRONG_PARAMS);
    }

    @Test
    public void testEditCandidateById() {
        UserResponse userResponse = insertUser("Ivan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user = new User(userResponse.getId(), userResponse.getFirstName(), userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber());
        CandidateResponse candidateResponse = insertCandidate(user, "Good program", ErrorCode.SUCCESS);
        editCandidateById(candidateResponse.getUser().getId(), candidateResponse.getUser(), "Bad program", ErrorCode.SUCCESS);
    }

    @Test
    public void testWrongEditCandidateById() {
        UserResponse userResponse = insertUser("Ivan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user = new User(userResponse.getId(), userResponse.getFirstName(), userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber());
        CandidateResponse candidateResponse = insertCandidate(user, "Good program", ErrorCode.SUCCESS);
        editCandidateById(candidateResponse.getUser().getId(), candidateResponse.getUser(), "", ErrorCode.WRONG_PARAMS);
    }

    @Test
    public void testEditVoteById() throws VotingException {
        UserResponse userResponse = insertUser("Ivan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user = new User(userResponse.getId(), userResponse.getFirstName(), userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber());
        CandidateResponse candidateResponse = insertCandidate(user, "Good program", ErrorCode.SUCCESS);
        Candidate candidate = new Candidate(candidateResponse.getUser(), candidateResponse.getProgram());
        VoteResponse voteResponse = insertVote(user, candidate, ErrorCode.SUCCESS);

        UserResponse newUserResponse = insertUser("Valentin", "Petrov", "Paclovich", "89078990909", ErrorCode.SUCCESS);
        User newUser = new User(newUserResponse.getId(), newUserResponse.getFirstName(), newUserResponse.getLastName(), newUserResponse.getPatronymic(), newUserResponse.getPhoneNumber());
        CandidateResponse newCandidateResponse = insertCandidate(newUser, "Good", ErrorCode.SUCCESS);
        Candidate newCandidate = new Candidate(newCandidateResponse.getUser(), newCandidateResponse.getProgram());

        editVoteById(voteResponse.getId(), user, newCandidate, ErrorCode.SUCCESS);
    }

    @Test
    public void testDeleteAllUsers() {
        insertUser("Ivan", "Ivanov", "Ivanovich", "89134567232", ErrorCode.SUCCESS);
        insertUser("Petr", "Petrov", "Petrovich", "89658529614", ErrorCode.SUCCESS);

        deleteAllUsers(ErrorCode.SUCCESS);
    }

    @Test
    public void testDeleteAllCandidates() {
        UserResponse userResponse = insertUser("Ivan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user = new User(userResponse.getId(), userResponse.getFirstName(), userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber());
        insertCandidate(user, "Good program", ErrorCode.SUCCESS);

        UserResponse newUserResponse = insertUser("Valentin", "Petrov", "Paclovich", "89078990909", ErrorCode.SUCCESS);
        User newUser = new User(newUserResponse.getId(), newUserResponse.getFirstName(), newUserResponse.getLastName(), newUserResponse.getPatronymic(), newUserResponse.getPhoneNumber());
        insertCandidate(newUser, "Good", ErrorCode.SUCCESS);

        deleteAllCandidates(ErrorCode.SUCCESS);
    }

    @Test
    public void testDeleteAllVotes() throws VotingException {
        UserResponse userResponse = insertUser("Ivan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user = new User(userResponse.getId(), userResponse.getFirstName(), userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber());
        CandidateResponse candidateResponse = insertCandidate(user, "Good program", ErrorCode.SUCCESS);
        Candidate candidate = new Candidate(candidateResponse.getUser(), candidateResponse.getProgram());
        insertVote(user, candidate, ErrorCode.SUCCESS);
        UserResponse newUserResponse = insertUser("Valentin", "Petrov", "Paclovich", "89078990909", ErrorCode.SUCCESS);
        User newUser = new User(newUserResponse.getId(), newUserResponse.getFirstName(), newUserResponse.getLastName(), newUserResponse.getPatronymic(), newUserResponse.getPhoneNumber());
        CandidateResponse newCandidateResponse = insertCandidate(newUser, "Good", ErrorCode.SUCCESS);
        Candidate newCandidate = new Candidate(newCandidateResponse.getUser(), newCandidateResponse.getProgram());
        insertVote(newUser, newCandidate, ErrorCode.SUCCESS);

        deleteAllVotes(ErrorCode.SUCCESS);
    }

    @Test
    public void testGetUsersByCandidateId1() throws VotingException {
        UserResponse userResponse = insertUser("Ivaan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user = new User(userResponse.getId(), userResponse.getFirstName(), userResponse.getLastName(), userResponse.getPatronymic(), userResponse.getPhoneNumber());
        CandidateResponse candidateResponse = insertCandidate(user, "Good program", ErrorCode.SUCCESS);
        Candidate candidate = new Candidate(candidateResponse.getUser(), candidateResponse.getProgram());
        UserResponse userResponse1 = insertUser("Ivaan", "Petrov", "Ivanovich", "89607584421", ErrorCode.SUCCESS);
        User user1 = new User(userResponse1.getId(), userResponse1.getFirstName(),userResponse1.getLastName(),userResponse1.getPatronymic(),userResponse1.getPhoneNumber());
        UserResponse newUserResponse = insertUser("Valentin", "Petrov", "Paclovich", "89078990909", ErrorCode.SUCCESS);
        User newUser = new User(newUserResponse.getId(), newUserResponse.getFirstName(), newUserResponse.getLastName(), newUserResponse.getPatronymic(), newUserResponse.getPhoneNumber());

        insertVote(user, candidate, ErrorCode.SUCCESS);
        insertVote(user1, candidate, ErrorCode.SUCCESS);
        insertVote(newUser, candidate, ErrorCode.SUCCESS);

        getUsersByCandidateId(candidate.getUser().getId(), 3, ErrorCode.SUCCESS);
    }

}