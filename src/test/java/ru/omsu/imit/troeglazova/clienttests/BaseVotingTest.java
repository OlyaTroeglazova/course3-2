package ru.omsu.imit.troeglazova.clienttests;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.troeglazova.client.VotingClient;
import ru.omsu.imit.troeglazova.dao.CandidateDAO;
import ru.omsu.imit.troeglazova.dao.VoteDAO;
import ru.omsu.imit.troeglazova.dao.UserDAO;
import ru.omsu.imit.troeglazova.daoimpl.*;
import ru.omsu.imit.troeglazova.exception.VotingException;
import ru.omsu.imit.troeglazova.model.Candidate;
import ru.omsu.imit.troeglazova.model.User;
import ru.omsu.imit.troeglazova.rest.request.*;
import ru.omsu.imit.troeglazova.rest.response.*;
import ru.omsu.imit.troeglazova.server.VotingServer;
import ru.omsu.imit.troeglazova.server.config.Settings;
import ru.omsu.imit.troeglazova.utils.ErrorCode;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BaseVotingTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseVotingTest.class);

    protected static VotingClient client = new VotingClient();
    private static String baseURL;

    private static void initialize() {
        String hostName = null;
        try {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
            LOGGER.debug("Can't determine my own host name", e);
        }
        baseURL = "http://" + hostName + ":" + Settings.getRestHTTPPort() + "/api";
    }

    @BeforeClass
    public static void startServer() {
        initialize();
        VotingServer.createServer();
    }

    @AfterClass
    public static void stopServer() {
        VotingServer.stopServer();
    }

    @Before
    public void clearDataBase() throws VotingException {
        VoteDAO voteDAO = new VoteDAOImpl();
        UserDAO userDAO = new UserDAOImpl();
        CandidateDAO candidateDAO = new CandidateDAOImpl();

        try {
            voteDAO.deleteAllVotes();
            userDAO.deleteAllUsers();
            candidateDAO.deleteAllCandidates();
        } catch (VotingException ex) {
            LOGGER.debug("VotingException in delete methods", ex);
        }
    }

    public static String getBaseURL() {
        return baseURL;
    }

    protected void checkFailureResponse(Object response, ErrorCode expectedStatus) {
        assertTrue(response instanceof FailureResponse);
        FailureResponse failureResponseObject = (FailureResponse) response;
        assertEquals(expectedStatus, failureResponseObject.getErrorCode());
    }

    protected UserResponse insertUser(String firstName, String lastName, String patronymic, String phoneNumber, ErrorCode expectedStatus) {
        UserRequest request = new UserRequest(firstName, lastName, patronymic, phoneNumber);
        Object response = client.post(baseURL + "/voting/user/", request, UserResponse.class);
        if (response instanceof UserResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            UserResponse addUserResponse = (UserResponse) response;
            Assert.assertEquals(firstName, addUserResponse.getFirstName());
            Assert.assertEquals(lastName, addUserResponse.getLastName());
            Assert.assertEquals(patronymic, addUserResponse.getPatronymic());
            Assert.assertEquals(phoneNumber, addUserResponse.getPhoneNumber());
            return addUserResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected CandidateResponse insertCandidate(User user, String program, ErrorCode expectedStatus) {
        CandidateRequest request = new CandidateRequest(user, program);
        Object response = client.post(baseURL + "/voting/candidate/", request, CandidateResponse.class);
        if (response instanceof CandidateResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            CandidateResponse addCandidateResponse = (CandidateResponse) response;
            Assert.assertEquals(user, addCandidateResponse.getUser());
            Assert.assertEquals(program, addCandidateResponse.getProgram());
            return addCandidateResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected VoteResponse insertVote(User user, Candidate candidate, ErrorCode expectedStatus) {
        VoteRequest request = new VoteRequest(user, candidate);
        Object response = client.post(baseURL + "/voting/vote/", request, VoteResponse.class);
        if (response instanceof VoteResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            VoteResponse addVoteResponse = (VoteResponse) response;
            Assert.assertEquals(user, addVoteResponse.getUser());
            Assert.assertEquals(candidate, addVoteResponse.getCandidate());
            return addVoteResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected UserResponse getUserById(int id, String expectedFirstName, String expectedLastName,
                                        String expectedPatronymic, String expectedPhoneNumber, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/voting/user/" + id, UserResponse.class);
        if (response instanceof UserResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            UserResponse getUserResponse = (UserResponse) response;
            Assert.assertEquals(id, getUserResponse.getId());
            Assert.assertEquals(expectedFirstName, getUserResponse.getFirstName());
            Assert.assertEquals(expectedLastName, getUserResponse.getLastName());
            Assert.assertEquals(expectedPatronymic, getUserResponse.getPatronymic());
            Assert.assertEquals(expectedPhoneNumber, getUserResponse.getPhoneNumber());
            return getUserResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected CandidateResponse getCandidateById(int id, User expectedUser, String expectedProgram, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/voting/candidate/" + id, CandidateResponse.class);
        if (response instanceof CandidateResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            CandidateResponse getCandidateResponse = (CandidateResponse) response;
            Assert.assertEquals(id, getCandidateResponse.getUser().getId());
            Assert.assertEquals(expectedUser, getCandidateResponse.getUser());
            Assert.assertEquals(expectedProgram, getCandidateResponse.getProgram());
            return getCandidateResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected VoteResponse getVoteById(int id, User expectedUser, Candidate expectedCandidate, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/voting/vote/" + id, VoteResponse.class);
        if (response instanceof VoteResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            VoteResponse getVoteResponse = (VoteResponse) response;
            Assert.assertEquals(id, getVoteResponse.getId());
            Assert.assertEquals(expectedUser, getVoteResponse.getUser());
            Assert.assertEquals(expectedCandidate, getVoteResponse.getCandidate());
            return getVoteResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteUserById(int id, ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/voting/user/del/" + id, EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteCandidateById(int id, ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/voting/candidate/del/" + id, EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteVoteById(int id, ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/voting/vote/del/" + id, EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected UserResponse editUserById(int id, String firstName, String lastName,
                                         String patronymic, String phoneNumber, ErrorCode expectedStatus) {
        UserRequest request = new UserRequest(firstName, lastName, patronymic, phoneNumber);
        Object response = client.put(baseURL + "/voting/user/edit/" + id, request, UserResponse.class);
        if (response instanceof UserResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            UserResponse editUserResponse = (UserResponse) response;
            Assert.assertEquals(firstName, editUserResponse.getFirstName());
            Assert.assertEquals(lastName, editUserResponse.getLastName());
            Assert.assertEquals(patronymic, editUserResponse.getPatronymic());
            Assert.assertEquals(phoneNumber, editUserResponse.getPhoneNumber());
            return editUserResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected CandidateResponse editCandidateById(int id, User user, String program, ErrorCode expectedStatus) {
        CandidateRequest request = new CandidateRequest(user, program);
        Object response = client.put(baseURL + "/voting/candidate/edit/" + id, request, CandidateResponse.class);
        if (response instanceof CandidateResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            CandidateResponse editCandidateResponse = (CandidateResponse) response;
            Assert.assertEquals(user, editCandidateResponse.getUser());
            Assert.assertEquals(program, editCandidateResponse.getProgram());
            return editCandidateResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected VoteResponse editVoteById(int id, User user, Candidate candidate, ErrorCode expectedStatus) {
        VoteRequest request = new VoteRequest(user, candidate);
        Object response = client.put(baseURL + "/voting/vote/edit/" + id, request, VoteResponse.class);
        if (response instanceof VoteResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            VoteResponse editVoteResponse = (VoteResponse) response;
            Assert.assertEquals(user, editVoteResponse.getUser());
            Assert.assertEquals(candidate, editVoteResponse.getCandidate());
            return editVoteResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteAllUsers(ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/voting/user/del/", EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteAllCandidates(ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/voting/candidate/del/", EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deleteAllVotes(ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/voting/vote/del/", EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected List<UserResponse> getUsersByCandidateId(int candidateId, int expectedCount, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/voting/user/getByCandidateId/" + candidateId, List.class);
        if (response instanceof List<?>) {
            Assert.assertEquals(ErrorCode.SUCCESS, expectedStatus);
            @SuppressWarnings("unchecked")
            List<UserResponse> responseList = (List<UserResponse>) response;
            Assert.assertEquals(expectedCount, responseList.size());
            return responseList;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }
}